#!/usr/bin/python
# =============================================================================
#        File : simpleserver.py
# Description : simple server echos the input
#      Author : Drew Gislsason
#        Date : 4/6/2017
# =============================================================================

# import standard items
import random
import string
import json
import sys

PORT = 5000

# import specialty items
from flask import Flask, request
# you can now refer to these objects by their name, instead of using the flask
# header (flask.request becomes just request)

# ============================== APIs ====================================

# create the global objects
app = Flask(__name__)

# Create a route
@app.route("/my/api", methods=['GET', 'PUT', 'POST', 'DELETE'])
def ApiV1Echo():

  # Create a text response variable rsp_data
  rsp_data = "Echo:\nMethod: "

  # append the method to your response variable
  if request.method == 'GET':
    rsp_data += "GET"
  elif request.method == 'PUT':
    rsp_data += "PUT"
  elif request.method == 'POST':
    rsp_data += "POST"
  elif request.method == 'DELETE':
    rsp_data += "DELETE"

  # append the argument portion to the response variable
  rsp_data += "\nArgs: "
  for arg in request.args:
    rsp_data += str(arg) + ":" + request.args[arg] + " "

  # append the data portion to the response variable
  rsp_data += "\nData:\n"
  rsp_data += request.get_data()
  rsp_data += "\n"

  # return the response variable and status code
  #    200 codes are all success
  #    300 codes are redirects
  #    400 codes are client errors
  return rsp_data, 200


# ============================== Main ====================================

if __name__ == "__main__":
  # when running Flask, turn on debug so that Flask restarts automatically
  app.debug = True
  # Use host = 0.0.0.0 if you want it open to anyone.
  # Use host = 127.0.0.1 if you want it open to localhost only.
  app.run(host='0.0.0.0', port=PORT)
