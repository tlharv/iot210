[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)

# Lab3 Part A - Google Protobufs and How to Use Them

Total Lab Time: 45 minutes

In this section of the lab you will

* Learn what Google protobufs are, and when to use them
* Learn what a .proto file is, and how to design one
* How to install Google protobufs on the Pi for Python
* Take the quiz using protobufs (instead of JSON)

## Some Useful Protobuf Links

**Tutorials**  
https://developers.google.com/protocol-buffers/
https://developers.google.com/protocol-buffers/docs/pythontutorial

**Source**  
https://github.com/google/protobuf
https://github.com/google/protobuf/releases

## Step1 - Verify you have enough disk space (you'll need about 2G free)

Open a terminal window on your RPi3 with ssh. Then check disk space.

```
$pi df -h
Filesystem      Size  Used Avail Use% Mounted on
/dev/root        59G  4.2G   52G   8% /
devtmpfs        459M     0  459M   0% /dev
tmpfs           463M     0  463M   0% /dev/shm
tmpfs           463M  6.3M  457M   2% /run
tmpfs           5.0M  4.0K  5.0M   1% /run/lock
tmpfs           463M     0  463M   0% /sys/fs/cgroup
/dev/mmcblk0p1   63M   21M   43M  34% /boot
tmpfs            93M     0   93M   0% /run/user/1000
```

Make sure you have at least 2G Avail on /dev/root.

## Step 2 - Install Google Protobufs 3.2 on your Raspberry Pi

Open another ssh terminal into the RPi3.

A release file from `https://github.com/google/protobuf/releases`
is already in the git project. Make sure to pull the latest.

Then, unzip that project into a new folder.

```
pi$ cd ~/Documents/Git/iot-210B-student
pi$ git pull
pi$ cd ~/Documents
pi$ unzip Git/iot-210B-student/Lab3/src/protobuf-3.2.0.zip
pi$ ls protobuf-3.2.0
```
You should see many files/folders in folder `protobuf-3.2.0`.


## Step 3 - Install the tools needed to build protobufs

The RPi3 by default doesn't have autoconf, automake or libtool installed.
Install those.

```
pi$ sudo apt-get update
pi$ sudo apt-get install autoconf automake libtool
```

# Step 4 - Building protoc

The configure is fairly fast.

The **make takes a REALLY LONG TIME** on the RPi. It took 7 minutes on my MAC. It
took over 45 minutes on my Pi. We'll stop here and wait for it to complete. While
we're waiting, more lecture!

```
pi$ cd ~/Documents/protobuf-3.2.0
pi$ ./configure
pi$ make
```

# Step 5 - Installing protoc

Once done making, then install protoc.

```
pi$ sudo make install
pi$ sudo ldconfig # refresh shared library cache.
pi$ protoc --version
    libprotoc 3.2.0 
```

# Step 6 - Add Protobufs to Python

Next, you need to add protobufs to your python folders. This should take
about 3 minutes.

```
pi$ cd ~/Documents/protobuf-3.2.0/python
pi$ python setup.py build
pi$ python setup.py test
pi$ sudo python setup.py install
```

# Step 7 - Running the Address Book Application

Now that everything is installed, you can actually use protobufs in your RPi3.

```
pi$ python list_people.py book
Person ID: 1
  Name: Drew
  E-mail address: drewg@rockisland.com
  Home phone #: 555-1212
Person ID: 2
  Name: Alicia Gislason
  E-mail address: alicigislason@icloud.com
  Work phone #: 444-2222
```

Or try adding an entry

```
pi$ python add_person.py
```

# Step 8 - Try Creating a Protobuf Program from Scratch

The following simple.proto file defines a single integer. 

```
syntax = "proto3";
message Test1 {
  int32 a = 1;
}
```

The program simple.py won't run until a file called "simple_pb2.py" is
created.

```
pi$ python simple.py
```

Notice it fails to run (can't import simple_pb2.py). Now, compile the
simple.proto file into a python module.

```
pi$ protoc --python_out=. simple.proto
pi$ python simple.py
    Enter number: 150
    created 'simple.bin'
```

Use hexdump to display the binary file `simple.bin`. If you set
`Test1.a = 150`, should be the bytes `08 96 01`.

```
hexdump simple.bin
```

Try running the program and entering other values. Try changing the
messgage Test1 in 'simple.proto' and modifying the Python program to
enter other things.

Next Lab:  
[PART B](https://gitlab.com/Gislason/iot-210B-student/tree/master/Lab3/LabPartB.md) Lab3 Part B - JSON, XML and Schemas

[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)
