[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)

# Week 3 - Google Protobufs, JSON vs XML, Schemas, SSE vs Websockets, MQTT with MQTT-SN, 

## Lab Objectives

In this lab you'll learn:

* How to add Google protobufs into Python, how to create .proto files and use them in applications.
* The difference between JSON and XML.
* How to use JSON and XML schemas.
* How to use Websockets and SSE.
* How to connect an MQTT-SN broker to an MQTT broker to support lossy networks.


## Homework

Set up an account and install ZigBee Home Automation 1.2 from www.ti.com. 

## Lab Links

[PART A](https://gitlab.com/Gislason/iot-210B-student/tree/master/Lab3/LabPartA.md) Google Protobufs and how to use them  
[PART B](https://gitlab.com/Gislason/iot-210B-student/tree/master/Lab3/LabPartB.md) JSON,XML and Schemas  
[PART C](https://gitlab.com/Gislason/iot-210B-student/tree/master/Lab3/LabPartC.md) WebSockets and SSE  
[PART D](https://gitlab.com/Gislason/iot-210B-student/tree/master/Lab3/LabPartD.md) MQTT and MQTT-SN  
[Homework](https://gitlab.com/Gislason/iot-210B-student/tree/master/Lab2/homework.md) Install ZigBee Home Automation 1.2  

[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)
