[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)

# Lab3 Part C - WebSockets and Server Sent Events

In this section, you'll learn

* How to use WebSockets
* How to use SSE

git clone https://github.com/websocket-client/websocket-client

# Step 1 - Install Webcosket

See: https://github.com/websocket-client/websocket-client

Open a terminal window into the Pi3 and install the websocket-client

```
pi$ cd ~/Documents
pi$ git clone https://github.com/websocket-client/websocket-client.git
pi$ cd websocket-client
pi$ sudo python setup.py install
```

Note: on my Mac, I apparently had a different websocket client installed
for Python. So I ended up downloading this package and using git clone.


# Step 2 - Use a WebSocket Client

A convenient WebSocket server is at `ws://echo.websocket.org/`

From the open terminal window on the Pi.

```
pi$ cd ~/Documents/Git/iot-210B-student/Lab3/src
pi$ ws_client.py
```

# Step 3 - SSE

Below is a curl script that will work with an SSE Client. However, I
wasn't able to find an on-line SSE Server (and didn't make one).

The `http_client.py` can easily be modified to perform the SSE client
functionality (just an added header, and keep open the socket).

I'll leave task to the student (optional).

```
pi$ curl host_ip/path/to/sse -H 'Accept: text/event-stream'
```

You can see one that works in your browser at:
`http://demo.howopensource.com/sse/`

You can also install flask-sse. I did not think we would have time to
install this and get it running with the other topics this lab.

See `https://pypi.python.org/pypi/Flask-SSE`


Next Lab:  
[PART D](https://gitlab.com/Gislason/iot-210B-student/tree/master/Lab3/LabPartD.md) Lab3 Part D - MQTT and MQTT-SN

[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)
