[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)

# Lab3 Part D - MQTT and MQTT-SN

In this section, you'll learn

* About the MQTT and MQTT-SN protocols
* How to access MQTT from the Web and from Python Applications

# Step 1 - Try out HiveMQ

MQTT is a widely adopted protocol (AWS uses it for example). A variety
of companies make MQTT broker and client (publisher and subscriber)
software.

HiveMQ is an online MQTT Broker that is enterprise quality.

1. Open a web browser to `http://www.hivemq.com/try-out/`
2. Click on the `Try the websocket client`
3. Pick a username/password
4. Set Keep Alive time to 300 (5 minutes) and Connect
5. You should see Topic `testtopic/1`
6. Add a subscription to `testtopic/1`
7. Type in a Message and Publish it
8. Let's all add a subscription to topic `iot210`
9. Type in Messages and Publish them to topic `iot210`

# Step 2 - Install Paho Python Client MQTT

```
pi$ sudo pip install pah-mqtt
```

Then, open 2 terminal windows on the Raspberry Pi, one for the publisher,
one for the subscriber (both of which are clients of a broker at iot.eclipse.org):

In the 1st terminal window, run the publisher

```
pi$ cd ~/Documents/Git/iot-210B-student/Lab3/src
pi$ python mqtt_publish.py
```

In the 2nd terminal window, run the subscriber

```
pi$ cd ~/Documents/Git/iot-210B-student/Lab3/src
pi$ python mqtt_publish.py
```

Try sending various messages to the iot210 group. Notice, we now have a
group chat.

# Step 3 - Extend the program

See if you can extend the program to allow topic: to go to a specific topic
to allow for individual or other group chats.

Example

```
Enter topic: string to post to individuals or other groups
```

# Step 4 - MQTT-SN

We'll learn about this in lecuture, but will implement during the ZigBee
or more embedded portion.


[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)
