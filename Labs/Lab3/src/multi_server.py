#!/usr/bin/python
# =============================================================================
#        File : multi_proto_server.py
# Description : mulitple protocol server support JSON, XML and Google Protobufs
#      Author : Drew Gislsason
#        Date : 4/9/2017
# =============================================================================
import string
import json
import sys
import base64
import binascii
import xml.etree.ElementTree as ET

import simple_pb2
from flask import Flask, request

# ============================== Data ====================================
PORT = 5000
content_json      = 'application/json'
content_xml       = 'application/xml'
content_protobufs = 'application/protobuf'

# Content-Types
type_JSON     = 0
type_XML      = 1
type_PROTOBUF = 2

# ============================== APIs ====================================

# create the global objects
app = Flask(__name__)

number = 1

# gets an input of a single number. Returns it in JSON.
@app.route("/api/v1/simple", methods=['GET', 'PUT'])
def ApiSimple():
  global number


  # assume JSON
  content_type = type_JSON
  if 'Content-Type' in request.headers:
    print "Content-Type: " + request.headers['Content-Type']
    if request.headers['Content-Type'] == content_xml:
      content_type = type_XML
    elif request.headers['Content-Type'] == content_protobufs:
      content_type = type_PROTOBUF
    elif request.headers['Content-Type'] == content_json:
      content_type = type_JSON

  # get (based on Content-Type)
  if request.method == 'GET':
    input_valid = True

  # put (based on Content-Type)
  if request.method == 'PUT':
    input_valid = False

    # '{"number":23}'
    if content_type == type_JSON:
      data = request.get_json(force=True,silent=True)
      if "number" in data:
        number = data["number"]
        input_valid = True

    # '<number>53</number>'
    elif content_type == type_XML:
      data = request.get_data()
      root = ET.fromstring(data)
      if root.tag == 'number':
        number = int(root.text)
        input_valid = True

    # base64 'CJYB'
    elif content_type == type_PROTOBUF:
      data = request.get_data()
      mystr = base64.b64decode(data)
      myvar = simple_pb2.Test1()
      myvar.ParseFromString(mystr)
      number = myvar.a
      input_valid = True

  if not input_valid:
    return "Bogus, Dude!\n", 400

  print "number = " + str(number)

  # return the number in XML format
  if content_type == type_XML:
    rsp_data = '<number>' + str(number) + '</number>\n'

  # return the number in Google Protobuf format
  elif content_type == type_PROTOBUF:
    myvar = simple_pb2.Test1()
    myvar.a = number
    mystr = myvar.SerializeToString()
    rsp_data = base64.b64encode(mystr)

  # assume JSON by default
  else:
    rsp_data = '{"number":' + str(number) + '}\n'
  
  return rsp_data, 200


# ============================== Main ====================================

if __name__ == "__main__":

  app.debug = True
  app.run(host='0.0.0.0', port=PORT)
