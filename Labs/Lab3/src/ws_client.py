# pip install websocket-client
import websocket
ws = websocket.create_connection("ws://echo.websocket.org/")

print "\n\nws_client from IoT 210 class\n"
print "Type exit to leave cleanly",
while True:
  s = raw_input("\nEnter a string: ")
  if s == 'exit':
    break
  ws.send(s)
  print "Receiving..."
  result =  ws.recv()
  print "Received (" + str(len(result)) + ") bytes '" + str(result) + "'"
ws.close()
