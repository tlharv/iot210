[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)

# Lab3 Part B - JSON, XML and Schemas

In this section of the lab you will create a Flask API that can take
protobufs, XML or JSON as input and output.

* Learn to convert between JSON and XML
* Learn to use a schema
* Learn how to handle JSON, XML and Protobufs in one Flask API
* Learn base64

# Step 1 - JSON, XML Protobuf Protocols

Examine the files `book.json`, `book.xml` and `book`. All three
files contain the same data encoded with different protocols.

* book.json - JSON format of address book
* book.xml - XML format of address book
* book - protobufs format of address book

```
cd ~/Documents/Git/iot-210B-student/Lab3/src
pi$ more book.json
pi$ more book.xml
pi$ hexdump book
```

# Step 2 - On-Line Validating

The following links define the JSON, XML and Protobug protocols.

http://www.json.org  
https://www.w3schools.com/xml/
https://developers.google.com/protocol-buffers/docs/pythontutorial
https://developers.google.com/protocol-buffers/docs/proto3

The following links allow you to validate data.

http://www.freeformatter.com/xml-validator-xsd.html  
http://jsonlint.com  
http://yura415.github.io/js-protobuf-encode-decode/  

Try creating some of your own JSON, XML and Protobuf Message files.

# Step 2 - Schemas

A Schema validates the content of your message, to make sure the
correct fields are present and the data is in the proper form.

Use the JSON file `person.json' and validate it against the schema
`person.jsd` in the online formatter below.

Use the XML file `order.xml` and validate it against the schema
`order.xsd`.

https://jsonschemalint.com/#/version/draft-04/markup/json
http://www.freeformatter.com/xml-validator-xsd.html  

There is no "schema" for protobufs that I'm aware of.

Try making modifications to the schema or the data. Make it wrong
and see if the validators catch it.


# Step 3 - Run multi_server, a Multiple Protocol Server

The python program `multi_server` can serve up JSON, XML and Protobuf
formatted data.

**API:**  
See simple.proto  

VERB | Route | Description | Data
---- | ----- | ----------- | ----
GET | /api/v1/simple | Get current number in (defaults to JSON format) | '{"number":3}'
PUT | /api/v1/simple | Set current number in (defaults to JSON format) | '{"number":3}'

In one terminal window on RPi3, run the multiple protocol server `multi_server.py`.

```
pi$ cd ~/Documents/Git/iot-210B-student/Lab3/src
pi$ python multi_server.py
```

In another terminal window on the RPi3 or your PC, try sending data in various forms.

Examples of payloads to the multiple protocol server.

```
// get current number
pi$ curl 127.1:5000/api/v1/simple

// set current number to 23
pi$ curl 127.1:5000/api/v1/simple -X 'PUT' -d '{"number":23}' -H 'Content-Type: application/json' --verbose

// set current number to 53 in XML
pi$ curl 127.1:5000/api/v1/simple -X 'PUT' -d '<number>53</number>' -H 'Content-Type: application/xml' --verbose

// set current number to 150 in Protobufs
pi$ curl 127.1:5000/api/v1/simple -X 'PUT' -d 'CJYB' -H 'Content-Type: application/protobuf' --verbose
```

# Step 4 - Base64

Base64 is an ASCII text represenation of binary data. It uses 64 ASCII character
(a-z,A-Z,0-9,+/) to encode the binary data. The resulting data is 33% larger,
but can be transmitted/parsed by ASCII protocols such as HTTP.

You can manually create a base64 representation of a protobuf Test1.a number by
doing the following in Python Idle.

```
pi$ python
import base64
str = '\x00\x11\x22\xfe'
str
data = base64.b64encode(str)
data
```

Protobufs is binary protocol. The following example assumes you've created
the simple_pb2.py from LabPartA.

```
pi$ python
import base64
import simple_pb2
myvar = simple_pb2.Test1()
myvar.a = 150
str = myvar.SerializeToString()
str
data = base64.b64encode(str)
data
```

Next Lab:  
[PART C](https://gitlab.com/Gislason/iot-210B-student/tree/master/Lab3/LabPartC.md) Lab3 Part C - WebSockets and Server Sent Events

[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)
