[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)

# Homework

The homework is to install the Texas Instruments ZigBee Software on your
Microsoft **Windows** laptop. If you run macOS or Linux, you'll need to
install a Windows virtual machine.

Many of the embedded tools run on Windows only (some only on older versions
such as Window 7 or even Windows XP). All the tools we use in class should
run on Windows 10 just fine.

# Step 1 - Hardware - CC2650 LaunchPad

You'll need at least one Texas Instruments CC2650 Launchpad. If working
remotely, get 2.

I recommend you order from DigiKey (as you can get it in a couple of days or less).

https://www.digikey.com/en/product-highlight/t/texas-instruments/launchxl-cc2650-launchpad-kit

See also: http://www.ti.com/tool/launchxl-cc2650

# Step 2 - Download the SimpleLink App in your Phone

If you have a smart phone (Android or Apple), you can Download
the Texas Instruments `Simplelink Starter` app from Apple App
Store or Google Play.

If you have the CC2650 LaunchPad hardware, you can verify it all
works, as it has a Bluetooth application ready to go out-of-box.

1. Plug LaunchPad into power (you can use one of your USB chargers or your laptop
2. Launch the SimpleLink Starter app
3. Press (and hold!) the BTN-2 button on the LaunchPad
4. Select the LaunchPad from the menu of Bluetooth Smart Devices
5. Select Mission Control
6. Turn on/off LEDs
7. Try out buttons (it will indicate on screen the state of BTN-1 and 2


# Step 3 - Download and Install Texas Instruments Software

http://processors.wiki.ti.com/index.php/CC2650_LaunchPad_User%27s_Guide_for_ZigBee

You'll need to install Z-Stack Home 1.2.2a SDK.

At this point, just download and install the Windows software.

We'll be going through setting up the actual LaunchPad boards with ZigBee
programs in class.


# Step 4 - Download and Install IAR

IAR is a C compiler and debugging environment.

If you already have IAR 7.40.2 or later, you are fine.

https://www.iar.com

IAR has a 30-day free trial. If you've already used a 30-day trial and can't
install it, call IAR and explain your are taking the UW IoT 210 course and
they should give you another 30-day free trial.

# Step 5 - Download and Install Ubiqua Protocol Analyzer

www.ubilogix.com

Download 30-Day free trial




[IOT STUDENT HOME](https://gitlab.com/Gislason/iot-210B-student/blob/master/README.md)
